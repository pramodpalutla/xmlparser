//
//  DetailViewController.swift
//  XMLParse
//
//  Created by Pramod Palutla on 20/11/17.
//  Copyright © 2017 Versant Online Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import WebKit



class DetailViewController: UIViewController {
    
    var webView = WKWebView()
    var descriptionString = String()
    // test
    override func viewDidLoad() {
        super.viewDidLoad()
        webView = WKWebView(frame: self.view.frame)
        self.view.addSubview(webView)
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        webView.loadHTMLString(descriptionString, baseURL: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
