//
//  ViewController.swift
//  XMLParse
//
//  Created by Pramod Palutla on 20/11/17.
//  Copyright © 2017 Versant Online Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController, XMLParserDelegate {
    
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var postTitle = NSMutableString()
    var postDescription = String()
    var date = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        beginParsing()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"https://gem-magazine.com/feed/"))! as URL)!
        parser.delegate = self
        parser.parse()
//        tbData!.reloadData()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            elements = NSMutableDictionary()
            elements = [:]
            postTitle = NSMutableString()
            postTitle = ""
            date = String()
            date = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "title") {
            postTitle.append(string)
        } else if element.isEqual(to: "pubDate") {
            date.append(string)
        } else if element.isEqual(to: "content:encoded") {
            postDescription.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if (elementName as NSString).isEqual(to: "item") {
            if !postTitle.isEqual(nil) {
                elements.setObject(postTitle, forKey: "title" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "date" as NSCopying)
            }
            if !postDescription.isEqual(nil) {
                elements.setObject(postDescription, forKey: "content" as NSCopying)
            }
            
            posts.add(elements)
//            print(posts)
        }
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        let post = posts[indexPath.row] as! NSDictionary
        print(post)
        cell.textLabel?.text = post.value(forKey: "title") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row] as! NSDictionary
        let detailVC = DetailViewController()
        detailVC.descriptionString = post.value(forKey: "content") as! String
        self.present(detailVC, animated: true, completion: nil)
    }
}

